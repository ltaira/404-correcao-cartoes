package br.com.mastertech.imersivo.cartao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.mastertech.imersivo.cartao.model.Cartao;
import br.com.mastertech.imersivo.cartao.service.CartaoService;

@RestController
public class CartaoController {
	
	@Autowired
	private CartaoService cartaoService;
	
	@GetMapping("/cartao/{numero}")
	public Cartao buscaCartao(@PathVariable String numero) {
		return cartaoService.buscaCartao(numero);
	}
	
	@GetMapping("/cartao/id/{id}")
	public Cartao buscaCartao(@PathVariable Long id) {
		return cartaoService.buscaCartao(id);
	}
	
	@PostMapping("/cartao")
	public Cartao criaCartao(@RequestBody Cartao cartao) {
		return cartaoService.criarCartao(cartao);
	}

}
