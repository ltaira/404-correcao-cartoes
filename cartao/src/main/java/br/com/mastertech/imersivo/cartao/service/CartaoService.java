package br.com.mastertech.imersivo.cartao.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.cartao.model.Cartao;
import br.com.mastertech.imersivo.cartao.repository.CartaoRepository;

@Service
public class CartaoService {

	@Autowired
	private CartaoRepository cartaoRepository;
	
	public Cartao criarCartao(Cartao cartao) {
		return cartaoRepository.save(cartao);
	}
	
	public Cartao buscaCartao(String numero) {
		return cartaoRepository.findByNumero(numero);
	}

	public Cartao buscaCartao(Long id) {
		Optional<Cartao> optional = cartaoRepository.findById(id);
		
		if(!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		
		return optional.get();
	}
	
}
